#include <iostream>

#define MAX_LENGHT 25

using namespace std;

__global__ void GetProd(int* x, int* y, int* z)
{
	int index = blockIdx.x;
	if (index < MAX_LENGHT)
	{
		int p = x[index] * y[index];
		z[index] = p;
	}
}

int main(int argc, char** argv)
{
	cout << "Product of two lists in other..." << endl;

	int x[MAX_LENGHT], y[MAX_LENGHT], z[MAX_LENGHT];

	for(int i = 0; i < MAX_LENGHT; i++)
	{
		x[i] = i;
		y[i] = i;
	}

	int *d_x, *d_y, *d_z;
	cudaMalloc((void**)&d_x, MAX_LENGHT * sizeof(int));
	cudaMalloc((void**)&d_y, MAX_LENGHT * sizeof(int));
	cudaMalloc((void**)&d_z, MAX_LENGHT * sizeof(int));

	cudaMemcpy(d_x, x, MAX_LENGHT * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_y, y, MAX_LENGHT * sizeof(int), cudaMemcpyHostToDevice);

	GetProd<<<MAX_LENGHT, 1>>>(d_x, d_y, d_z);

	cudaMemcpy(z, d_z, MAX_LENGHT * sizeof(int), cudaMemcpyDeviceToHost);

	for(int i = 0; i < MAX_LENGHT; i++)
	{
		cout << i << " * " << i << " = " << z[i] << endl;
	}

	cudaFree(d_x);
	cudaFree(d_y);
	cudaFree(d_z);

	return 0;
}

/*
void GetProd(int* x, int* y, int* z)
{
	for(int i = 0; i < MAX_LENGHT; i++)
	{
		z[i] = x[i] * y[i];
	}
}

int main(int argc, char** argv)
{
	cout << "Product of two lists in other..." << endl;

	int x[MAX_LENGHT], y[MAX_LENGHT], z[MAX_LENGHT];

	for(int i = 0; i < MAX_LENGHT; i++)
	{
		x[i] = i;
		y[i] = i;
	}

	GetProd(x, y, z);

	for(int i = 0; i < MAX_LENGHT; i++)
	{
		cout << i << " * " << i << " = " << z[i] << endl;
	}

	return 0;
}
*/
