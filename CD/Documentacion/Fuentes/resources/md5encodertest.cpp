/*
 * MD5EncoderTest.cpp
 *
 *  Created on: Oct 25, 2015
 *      Author: Cesar Flores
 */

#include "md5encodertest.h"

void MD5EncoderTest::SetUp()
{
	encoder = MD5EncoderFactory::GetMD5Encoder();
}

void MD5EncoderTest::TearDown()
{
#ifdef NVCC
	CU_MD5Encoder* toDelete = dynamic_cast<CU_MD5Encoder*>(encoder);
	delete toDelete;
#else
	SimpleMD5Encoder* toDelete = dynamic_cast<SimpleMD5Encoder*>(encoder);
	delete toDelete;
#endif
}

TEST_F(MD5EncoderTest, MemoryDigestCheckEmptyString)
{
	string hw = "";
	MD5MemoryInput input;
	input.input = (unsigned char*)hw.c_str();
	input.length = hw.size();
	vector<MD5MemoryInput> v;
	v.push_back(input);
	encoder->MemoryDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	ASSERT_EQ(1, digests.size());
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), ((const char*)input.input)) == 0)
		{
			ASSERT_EQ("d41d8cd98f00b204e9800998ecf8427e", (*it).digest);
		}
	}
}

TEST_F(MD5EncoderTest, MemoryDigestCheckSingleChar)
{
	string hw = "a";
	MD5MemoryInput input;
	input.input = (unsigned char*)hw.c_str();
	input.length = hw.size();
	vector<MD5MemoryInput> v;
	v.push_back(input);
	encoder->MemoryDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	ASSERT_EQ(1, digests.size());
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), ((const char*)input.input)) == 0)
		{
			ASSERT_EQ("0cc175b9c0f1b6a831c399e269772661", (*it).digest);
		}
	}
}

TEST_F(MD5EncoderTest, MemoryDigestCheckMultipleChar)
{
	string hw = "abc";
	MD5MemoryInput input;
	input.input = (unsigned char*)hw.c_str();
	input.length = hw.size();
	vector<MD5MemoryInput> v;
	v.push_back(input);
	encoder->MemoryDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	ASSERT_EQ(1, digests.size());
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), ((const char*)input.input)) == 0)
		{
			ASSERT_EQ("900150983cd24fb0d6963f7d28e17f72", (*it).digest);
		}
	}
}

TEST_F(MD5EncoderTest, MemoryDigestCheckAlphaAndNumber)
{
	string hw = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	MD5MemoryInput input;
	input.input = (unsigned char*)hw.c_str();
	input.length = hw.size();
	vector<MD5MemoryInput> v;
	v.push_back(input);
	encoder->MemoryDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	ASSERT_EQ(1, digests.size());
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), ((const char*)input.input)) == 0)
		{
			ASSERT_EQ("d174ab98d277d9f5a5611c2c9f419d9f", (*it).digest);
		}
	}
}

TEST_F(MD5EncoderTest, MemoryDigestCheckTwoMessages)
{
	vector<MD5MemoryInput> v;

	string hw = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	MD5MemoryInput input;
	input.input = (unsigned char*)hw.c_str();
	input.length = hw.size();
	v.push_back(input);

	string hw1 = "abc";
	MD5MemoryInput input1;
	input1.input = (unsigned char*)hw1.c_str();
	input1.length = hw1.size();
	v.push_back(input1);

	encoder->MemoryDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	ASSERT_EQ(2, digests.size());
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), ((const char*)input.input)) == 0)
		{
			ASSERT_EQ("d174ab98d277d9f5a5611c2c9f419d9f", (*it).digest);
		}

		if (strcmp(((const char*)(*it).input), ((const char*)input1.input)) == 0)
		{
			ASSERT_EQ("900150983cd24fb0d6963f7d28e17f72", (*it).digest);
		}
	}
}

TEST_F(MD5EncoderTest, MemoryDigestCheckMultipleMessages)
{
	vector<MD5MemoryInput> v;

	string hw = "";
	MD5MemoryInput input;
	input.input = (unsigned char*)hw.c_str();
	input.length = hw.size();
	v.push_back(input);

	string hw1 = "a";
	MD5MemoryInput input1;
	input1.input = (unsigned char*)hw1.c_str();
	input1.length = hw1.size();
	v.push_back(input1);

	string hw2 = "abc";
	MD5MemoryInput input2;
	input2.input = (unsigned char*)hw2.c_str();
	input2.length = hw2.size();
	v.push_back(input2);

	string hw3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	MD5MemoryInput input3;
	input3.input = (unsigned char*)hw3.c_str();
	input3.length = hw3.size();
	v.push_back(input3);

	encoder->MemoryDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	ASSERT_EQ(4, digests.size());
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), ((const char*)input.input)) == 0)
		{
			ASSERT_EQ("d41d8cd98f00b204e9800998ecf8427e", (*it).digest);
		}

		if (strcmp(((const char*)(*it).input), ((const char*)input1.input)) == 0)
		{
			ASSERT_EQ("0cc175b9c0f1b6a831c399e269772661", (*it).digest);
		}

		if (strcmp(((const char*)(*it).input), ((const char*)input2.input)) == 0)
		{
			ASSERT_EQ("900150983cd24fb0d6963f7d28e17f72", (*it).digest);
		}

		if (strcmp(((const char*)(*it).input), ((const char*)input3.input)) == 0)
		{
			ASSERT_EQ("d174ab98d277d9f5a5611c2c9f419d9f", (*it).digest);
		}
	}
}

// File digest check not supported by CUDA version yet.
#ifndef NVCC
TEST_F(MD5EncoderTest, SingleFileDigestCheck)
{
	string file = "resources/rfc1321-es.txt";
	vector<string> v;
	v.push_back(file);

	encoder->FileDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), file.c_str()) == 0)
		{
			ASSERT_EQ("bbf6d4313915fd51cdac6ed7b638aaf7", (*it).digest);
		}

	}
}

TEST_F(MD5EncoderTest, TwoFileDigestCheck)
{
	vector<string> v;
	string file = "resources/rfc1321-es.txt";
	v.push_back(file);

	string file1 = "resources/rfc1321-en.txt";
	v.push_back(file1);

	encoder->FileDigest(v);
	vector<MD5DigestOutput> digests = encoder->GetDigests();
	for (vector<MD5DigestOutput>::iterator it = digests.begin(); it != digests.end(); it++)
	{
		if (strcmp(((const char*)(*it).input), file.c_str()) == 0)
		{
			ASSERT_EQ("bbf6d4313915fd51cdac6ed7b638aaf7", (*it).digest);
		}

		if (strcmp(((const char*)(*it).input), file1.c_str()) == 0)
		{
			ASSERT_EQ("c6beb4140671d319f6433a3399cf6df2", (*it).digest);
		}
	}
}
#endif // ifdef NVCC
