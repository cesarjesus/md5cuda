			Readme
			======

Para poder ejecutar la implementación del algoritmo MD5 con soporte CUDA,
es necesario tener una tarjeta gráfica con chip NVIDIA, para el proyecto
se usó una GeForce GT 630 (rev a1). Todas la pruebas y la implementación
en sí estan pensadas para un entorno Unix/GNU Linux, para el desarrollo e
implementación se uso Ubuntu 14.04.

1.- Instalación de drivers.

	$ sudo ./NVIDIA-Linux-x86_64-331.67.run

1.- Instalacion de CUDA

	$ sudo ./cuda_6.5.14_linux_64.run

2.- Instalacion del Sistema

	$ sudo ./install.sh

