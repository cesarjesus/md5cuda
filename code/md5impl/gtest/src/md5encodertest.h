/*
 * md5encodertest.h
 *
 *  Created on: Oct 25, 2015
 *      Author: Cesar Flores
 */

#ifndef MD5ENCODERTEST_H_
#define MD5ENCODERTEST_H_

#include <iostream>
#include <vector>

#include "gtest/gtest.h"
#include "md5encoder.h"
#include "md5encoder_factory.h"

using namespace std;

class MD5EncoderTest : public ::testing::Test
{
public:
	MD5Encoder* encoder;

public:
	void SetUp();
	void TearDown();
};

#endif /* MD5ENCODERTEST_H_ */
