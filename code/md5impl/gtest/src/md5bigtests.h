#ifndef MD5BIGTESTS_H_
#define MD5BIGTESTS_H_

#include <iostream>
#include <vector>
#include <string>

#include "gtest/gtest.h"
#include "md5encoder.h"
#include "md5encoder_factory.h"

using namespace std;

class MD5BigTests : public ::testing::Test
{
public:
	MD5Encoder* encoder;

public:
	void SetUp();
	void TearDown();
};

#endif /* MD5BIGTESTS_H_ */
