//============================================================================
// Name        : main.cpp
// Author      : Cesar Flors
// Version     :
// Copyright   : 2015
// Description : Main file to run the tests.
//============================================================================

#include <iostream>

#include "gtest/gtest.h"

using namespace std;

int main(int argc, char** argv) {
	cout << "Running tests..." << endl;
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
