# CUDA home
CUDA_HOME = /usr/local/cuda

# NVCC
NVCC = $(CUDA_HOME)/bin/nvcc
NVCC_VERSION = $(shell $(NVCC) --version 2>/dev/null)
