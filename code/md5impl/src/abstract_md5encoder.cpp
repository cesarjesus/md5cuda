/*
 * abstract_md5encoder.cpp
 *
 *  Created on: Mar 31, 2015
 *      Author: Cesar Flores
 */

#include "abstract_md5encoder.h"

AbstractMD5Encoder::AbstractMD5Encoder()
{
}

AbstractMD5Encoder::~AbstractMD5Encoder()
{
}

void AbstractMD5Encoder::SetDigestChars(char tmpDgstChars[33], unsigned char bytes[16])
{
	memset(tmpDgstChars, 0, 32);
	for (int count = 0; count < 16; count++)
	{
		sprintf(tmpDgstChars + (count * 2), "%02x", bytes[count]);
	}
}

vector<MD5DigestOutput> AbstractMD5Encoder::GetDigests()
{
	return digests;
}
