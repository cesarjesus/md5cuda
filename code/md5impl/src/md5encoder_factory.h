#ifndef MD5ENCODER_FACTORY_H
#define MD5ENCODER_FACTORY_H

#include "md5encoder.h"
#include "simple_md5encoder.h"
#include "cu_md5encoder.cuh"

/*
 * Factory class to get the correct inplementation of the MD5Encoder.
 * At compile time if CUDA platform is found, the instance get will be
 * parallel processing capable, if CUDA is not found an simple implementation
 * will be returned. The instance get must be deleted by the user after use.
 * Ex.
 *      MD5Encoder* encoder = Factory::GetMD5Encoder();
 *      .....
 *      delete encoder;
 */
class MD5EncoderFactory
{
public:
    /*
     * Gets an instance of MD5Encoder based if CUDA platform is found or not.
     * The instance have to be deleted after use.
     * @return CU_MD5Encoder if CUDA platform is available, SimpleMD5Encoder
     * in other case.
     */
    static MD5Encoder* GetMD5Encoder();
};

#endif /* MD5ENCODER_FACTORY_H */
