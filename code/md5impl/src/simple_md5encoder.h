#ifndef SIMPLE_MD5ENCODER_H
#define SIMPLE_MD5ENCODER_H

#include "abstract_md5encoder.h"

/*
 * Non-parallel implementation of MD5 Encoder.
 * This implementation will be instantiated if at time to compile
 * the code the CUDA platform is not found or is not configured properly.
 */
class SimpleMD5Encoder: public AbstractMD5Encoder
{
private:
    /* Pointer to the internal context.*/
    MD5Context* context;
    char tmpDigestChars[33];

private:
    /* Initialize the internal context.*/
    void InitContext();

    /* Gets the current internal context.*/
	MD5Context* Context();

    /*
     * Calculate the digest for the given block of data, and put
     * the result in the internal MD5Context.
     * @param block, the block of data for which the digest will be calculated.
     * @param length, the size of the block.
     */
    void Update(unsigned char* block, unsigned int length);

    /*
     * Encode UINT4 data type, four bytes, to an unsigned char.
     * @param out, the pointer to the encoded value.
     * @param in, pointer to the UINT4 tha will be encoded.
     * @param len, size of the data block that will be encoded.
     */
    void Encode(unsigned char* out, UINT4 *in, unsigned int len);

    /* Finalize the digest calculation. */
    void Finalize();

    /*
     * Decode an unsigned char to UINT4. Transform a block of data represented
     * by unsigned char data type to UINT4 data type that is represented by
     * four bytes.
     * @param out, a pointer to the UINT4 block where will be stored the decoded data.
     * @param block, a pointer to unsigned char data type that will be decoded.
     * @param blockLengh, size of the block to be decoded.
     */
    void Decode(UINT4 *out, unsigned char* block, unsigned int blockLengh);

    /*
     * Makes the digest calculation for the given block and store the result
     * in the given state.
     * @param state, where the states of bytes ABCD will be stored.
     * @param block, that will be transformed.
     */
    void MD5Transform(UINT4 state[4], unsigned char block[64]);

    /* Get a pointer to the temporary digest that correspond to the current iteration.*/
    char* GetTmpDigest();

public:
    /* Default constructor, will create a new context and initizlize it.*/
    SimpleMD5Encoder();

    /* Destructor will delete the context.*/
    virtual ~SimpleMD5Encoder();

    /* Implements a memory digest that runs on CPU and is not parallel.*/
    virtual void MemoryDigest(vector<MD5MemoryInput> &inputList);

    /* Implements a file digest that runs on CPU and is not parallel.*/
    virtual void FileDigest(vector<string> &fileList);
};

#endif /* SIMPLE_MD5ENCODER_H */
