#ifndef MD5ENCODER_H
#define MD5ENCODER_H

#include <string>
#include <vector>

using namespace std;

/*
 * Constants for trnasform runtine,
 * its easier to se constants instead of just numbers.
 */
#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21

/* Define auxiliar functions */
#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | (~z)))

/* Rotate left x in n bits */
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

#define FF(a, b, c, d, x, s, ac) { \
  (a) += F ((b), (c), (d)) + (x) + (UINT4)(ac); \
  (a) = ROTATE_LEFT ((a), (s)); \
  (a) += (b); \
  }
#define GG(a, b, c, d, x, s, ac) { \
  (a) += G ((b), (c), (d)) + (x) + (UINT4)(ac); \
  (a) = ROTATE_LEFT ((a), (s)); \
  (a) += (b); \
  }
#define HH(a, b, c, d, x, s, ac) { \
  (a) += H ((b), (c), (d)) + (x) + (UINT4)(ac); \
  (a) = ROTATE_LEFT ((a), (s)); \
  (a) += (b); \
  }
#define II(a, b, c, d, x, s, ac) { \
  (a) += I ((b), (c), (d)) + (x) + (UINT4)(ac); \
  (a) = ROTATE_LEFT ((a), (s)); \
  (a) += (b); \
  }

/* Define a four bytes data type */
typedef unsigned int UINT4;

/* MD5Context to store internal states */
typedef struct {
	/* Store the state of the ABCD values*/
	UINT4 state[4];

	UINT4 count[2];

	/* Store the input buffer */
	unsigned char buffer[64];
} MD5Context;

/* Define memory input structure */
typedef struct {
    /* Pointer to the first position of the data. */
	unsigned char* input;
    /* Lenght of the data. */
	unsigned int length;
} MD5MemoryInput;

/* Define digest output structure */
typedef struct {
    /* Pointer to the first position of the data. */
	unsigned char* input;
    /* Disgest hexadecimal represented as printable
     characters encapsulated on string.*/
	string digest;
} MD5DigestOutput;

/*
 * Abstraction of MD5 Encoder with main functions defined.
 */
class MD5Encoder
{
public:
    /*
     * Calculates the digest of the given MD5MemoryInput.
     * @param inputList, a vector of MD5MemoryInput elements.
     */
    virtual void MemoryDigest(vector<MD5MemoryInput> &inputList) = 0;

    /*
     * Calculates the digest of the give list of files.
     * @param fileList, list of strings that represent a file names.
     */
    virtual void FileDigest(vector<string> &fileList) = 0;

    /*
     * Gets the list of MD5DigestOutput that contains the calculated
     * digets.
     * @return a list of MD5DigestOutput.
     */
    virtual vector<MD5DigestOutput> GetDigests() = 0;
};

#endif /* MD5ENCODER_H */
