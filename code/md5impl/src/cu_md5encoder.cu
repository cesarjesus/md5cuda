#include "cu_md5encoder.cuh"

/* Padding block array will be in the device. */
__device__ unsigned char CU_PADDING[64] = {
	0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/*
 * Device function. This function will be executed in the GPU.
 * @ref SimpleMD5Encoder#Decode
 */
__device__ void Decode(UINT4 *out, unsigned char* in, unsigned int blockLengh)
{
	unsigned int inCount, outCount;
	for (inCount = 0, outCount = 0; outCount < blockLengh; inCount++, outCount += 4)
	{
		out[inCount] = ((UINT4)in[outCount]) | (((UINT4)in[outCount + 1]) << 8 ) |
			(((UINT4)in[outCount + 2]) << 16) | (((UINT4)in[outCount + 3]) << 24);
	}
}

/*
 * Device function. This function will be executed in the GPU.
 * @ref SimpleMD5Encoder#Encode
 */
__device__ void Encode(unsigned char* out, UINT4 *in, unsigned int len)
{
	unsigned int inCount, outCount;
	for (inCount = 0, outCount = 0; outCount < len; inCount++, outCount += 4)
	{
		out[outCount]     = (unsigned char)(in[inCount] & 0xff);
		out[outCount + 1] = (unsigned char)((in[inCount] >> 8) & 0xff);
		out[outCount + 2] = (unsigned char)((in[inCount] >> 16) & 0xff);
		out[outCount + 3] = (unsigned char)((in[inCount] >> 24) & 0xff);
	}
}

/*
 * Device function. This function will be executed in the GPU.
 * @ref SimpleMD5Encoder#MD5Transform
 */
__device__ void MD5Transform(UINT4 state[4], unsigned char block[64])
{
	UINT4 a = state[0], b = state[1], c = state[2], d = state[3];
	UINT4 x[16];

	Decode(x, block, 64);

	/* Round 1 */
	FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
	FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
	FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
	FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
	FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
	FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
	FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
	FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
	FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
	FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
	FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
	FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
	FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
	FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
	FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
	FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

   /* Round 2 */
	GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
	GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
	GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
	GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
	GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
	GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
	GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
	GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
	GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
	GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
	GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
	GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
	GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
	GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
	GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
	GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

	/* Round 3 */
	HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
	HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
	HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
	HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
	HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
	HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
	HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
	HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
	HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
	HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
	HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
	HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
	HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
	HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
	HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
	HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */

	/* Round 4 */
	II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
	II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
	II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
	II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
	II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
	II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
	II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
	II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
	II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
	II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
	II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
	II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
	II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
	II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
	II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
	II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */

	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;

	/* Clean up sensitive date. */
	memset((unsigned char*)x, 0, sizeof(x));
}

/*
 * Device function. This function will be executed in the GPU.
 * Initialize the given context with magic numbers, this is the first step
 * of the MD5 algorithm.
 * @param context, reference to the MD5Context that will be initilized.
 */
__device__ void InitContext(MD5Context &context)
{
	context.state[0] = 0x67452301;
	context.state[1] = 0xefcdab89;
	context.state[2] = 0x98badcfe;
	context.state[3] = 0x10325476;

	context.count[0] = 0;
	context.count[1] = 0;
}

/*
 * Device function. This function will be executed in the GPU.
 * Makes the digest calculation for the given block using the given
 * context as MD5Context.
 * @ref SimpleMD5Encoder#Update
 */
__device__ void Update(MD5Context &context, unsigned char* block, unsigned int length)
{
	unsigned int index, partLen, blockCount;

	index = (unsigned int)((context.count[0] >> 3) & 0x3F);

	if ((context.count[0] += ((UINT4)length << 3)) < ((UINT4)length << 3))
	{
		context.count[1]++;
	}
	context.count[1] += ((UINT4)length >> 29);

	partLen = 64 - index;

	if (length >= partLen)
	{
		memcpy((unsigned char*)&context.buffer[index], (unsigned char*)block, partLen);
		MD5Transform(context.state, context.buffer);

		/* Loop throug all byte blocks. Put here the parallel part */
		for (blockCount = partLen; blockCount + 63 < length; blockCount += 64)
		{
			MD5Transform(context.state, &block[blockCount]);
		}

		index = 0;
	}
	else
	{
		blockCount = 0;
	}

	memcpy((unsigned char*)&context.buffer[index], (unsigned char*)&block[blockCount], length - blockCount);
}

/*
 * Device function. This function will be executed in the GPU.
 * Finalize the digest calculation using the given context.
 * @ref SimpleMD5Encoder#Finalize
 */
__device__ void Finalize(MD5Context &context, unsigned char* digest)
{
	unsigned char bits[8];
	unsigned int index, partLen;
	Encode(bits, context.count, 8);

	index = (unsigned int)((context.count[0] >> 3) & 0x3f);
	partLen = (index < 56) ? (56 - index) : (120 - index);
	Update(context, CU_PADDING, partLen);

	Update(context, bits, 8);

	unsigned char digestRaw[16];
	Encode(digestRaw, context.state, 16);

	memset((unsigned char*)&context, 0, sizeof(context));
	memcpy(digest, digestRaw, 16);
}

/*
 * Device function. This function will be executed in the GPU.
 * Makes the digest calculation in parallel. The given parameters are pointers
 * to the device memory where the list of messages are stored, the function access
 * directly to the possition of an individual message by index that is calculated
 * according to the number of block in the GPU that it is being executed.
 * @param d_messages, a pointer to an unsigned char* that holds the message
 * for which the MD5 will be calculated.
 * @param d_sizes, a pointer to int that holds the size of the individual message.
 * @param d_digests, a pointer to unsigned char* where the calculated digests
 * will be stored.
 */
__global__ void MakeMD5Transform(unsigned char** d_messages, int* d_sizes, unsigned char** d_digests)
{
	int idx = blockIdx.x;
	MD5Context context;
	InitContext(context);
	if (d_sizes[idx] == 0)
	{
		Update(context, (unsigned char*)"", (unsigned int)d_sizes[idx]);
	}
	else
	{
		Update(context, d_messages[idx], (unsigned int)d_sizes[idx]);
	}
	Finalize(context, d_digests[idx]);
}

CU_MD5Encoder::CU_MD5Encoder()
{
}

CU_MD5Encoder::~CU_MD5Encoder()
{
}

/* Check the given error and print an error string if it is not success.*/
void checkError(cudaError_t error)
{
	if (error != cudaSuccess)
	{
		cout << "CUDA Error: " << cudaGetErrorString(error) << endl;
	}
}

/*
 * Calculate digests for the given list of inputs. Allocate device memory
 * and copy the message inputs to device, calculate the number of blocks that
 * will run in parallel and finally copy back from device to host the
 * calculated digests.
 * @ref MD5Encoder#MemoryDigest
 */
void CU_MD5Encoder::MemoryDigest(vector<MD5MemoryInput> &inputList)
{
	/* Number of messages will be the number of blocks.*/
	int nroMessages = inputList.size();

	/* Pointer to the messages in the host.*/
	unsigned char** h_messages = (unsigned char**)malloc(sizeof(char*) * nroMessages);

	/* Pointer to the digests in the host.*/
	unsigned char** h_digests = (unsigned char**)malloc(sizeof(char*) * nroMessages);

	/* Array with message sizes in the host.*/
	int h_sizes[nroMessages];

	int count = 0, msgSize = 0;
	cudaError_t error;
	/* Iterate over messages to allocate and copy resources to the device.*/
	for (vector<MD5MemoryInput>::iterator it = inputList.begin(); it != inputList.end(); it++)
	{
		msgSize = (*it).length;
		// We can't malloc 0 size
		if (msgSize > 0)
		{
			error = cudaMalloc((void**)&h_messages[count], sizeof(unsigned char) * msgSize);
			checkError(error);
			error = cudaMemcpy(h_messages[count], (*it).input, msgSize, cudaMemcpyHostToDevice);
			checkError(error);
		}

		error = cudaMalloc((void**)&h_digests[count], sizeof(unsigned char) * 16);
		checkError(error);

		h_sizes[count] = msgSize;
		count++;
	}

	/* Allocate and copy messages list to the device.*/
	unsigned char** d_messages;
	error = cudaMalloc((void***)&d_messages, sizeof(unsigned char*) * nroMessages);
	checkError(error);
	error = cudaMemcpy(d_messages, h_messages, sizeof(unsigned char*) * nroMessages, cudaMemcpyHostToDevice);
	checkError(error);

	/* Allocate and copy resources for the digests in the device.*/
	unsigned char** d_digests;
	error = cudaMalloc((void***)&d_digests, sizeof(unsigned char*) * nroMessages);
	checkError(error);
	error = cudaMemcpy(d_digests, h_digests, sizeof(unsigned char*) * nroMessages, cudaMemcpyHostToDevice);
	checkError(error);

	/* Allocate and copy sizes of the messages to the device.*/
	int* d_sizes;
	error = cudaMalloc((void**)&d_sizes, sizeof(int) * count);
	checkError(error);
	error = cudaMemcpy(d_sizes, h_sizes, sizeof(int) * count, cudaMemcpyHostToDevice);
	checkError(error);

	/* Call to the function that will execute in parallel.*/
	clock_t start = clock();
	MakeMD5Transform<<<nroMessages, 1>>>(d_messages, d_sizes, d_digests);
	std::cout << "CUDA MD5 elapsed time:" << float(clock() - start) / CLOCKS_PER_SEC << std::endl;
	error = cudaDeviceSynchronize();
	checkError(error);

	/* Iterate to get back the calculated digests from the device.*/
	unsigned char* digests_out[nroMessages];
	for (int i = 0; i < nroMessages; i++)
	{
		digests_out[i] = (unsigned char*)malloc(sizeof(unsigned char) * 16);
		error = cudaMemcpy(digests_out[i], h_digests[i], sizeof(unsigned char) * 16, cudaMemcpyDeviceToHost);
		checkError(error);

		char ddch[33];
		SetDigestChars(ddch, digests_out[i]);

		unsigned char* msg = (unsigned char*)malloc(sizeof(unsigned char) * h_sizes[i]);
		error = cudaMemcpy(msg, h_messages[i], h_sizes[i], cudaMemcpyDeviceToHost);
		checkError(error);

		MD5DigestOutput dOut;
		dOut.input = msg;
		dOut.digest = ddch;

		digests.push_back(dOut);
	}

	/* Release all allocated resources in the device and host.*/
	for (int i = 0; i < nroMessages; i++)
	{
		cudaFree(h_messages[i]);
		cudaFree(h_digests[i]);
		free(digests_out[i]);
	}

	cudaFree(d_messages);
	cudaFree(d_digests);
	cudaFree(d_sizes);

	free(h_messages);
	free(h_digests);
}

void CU_MD5Encoder::FileDigest(vector<string> &fileList)
{

}

vector<MD5DigestOutput> CU_MD5Encoder::GetDigests()
{
	return digests;
}
