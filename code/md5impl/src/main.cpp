/*
 * main.cpp
 *
 *  Created on: Mar 31, 2015
 *      Author: Cesar Flores
 */

#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <string>
#include <vector>

#include "md5encoder.h"
#include "md5encoder_factory.h"

using namespace std;

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		cout << "Usage:" << endl;
		cout << "\t" << argv[0] << " <string-1> <string-2> ... <string-n>" << endl;
		return 0;
	}

	vector<MD5MemoryInput> inputList;
	string tmp;
	for (int i = 1; i < argc; i++)
	{
		tmp = argv[i];
		MD5MemoryInput in;
		in.input = (unsigned char*)malloc(tmp.size() + 1);
		memset(in.input, 0, tmp.size() + 1);
		memcpy(in.input, (unsigned char*)tmp.c_str(), tmp.size());
		in.length = (unsigned int)tmp.size();
		inputList.push_back(in);
	}

	MD5Encoder* encoder = MD5EncoderFactory::GetMD5Encoder();

	encoder->MemoryDigest(inputList);
	vector<MD5DigestOutput> outDigest = encoder->GetDigests();
	vector<MD5DigestOutput>::iterator outIt;
	vector<MD5MemoryInput>::iterator inIt;
	for (outIt = outDigest.begin(), inIt = inputList.begin();
	 	outIt != outDigest.end(), inIt != inputList.end();
		outIt++, inIt++)
	{
		cout << (*inIt).input << "\t" << (*outIt).digest << endl;
	}

	for (inIt = inputList.begin(); inIt != inputList.end(); inIt++)
	{
		free((*inIt).input);
	}

#ifdef NVCC
	CU_MD5Encoder* toDelete = dynamic_cast<CU_MD5Encoder*>(encoder);
	delete toDelete;
#else
	SimpleMD5Encoder* toDelete = dynamic_cast<SimpleMD5Encoder*>(encoder);
	delete toDelete;
#endif

	return 0;
}
