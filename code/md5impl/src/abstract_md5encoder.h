/*
 * abstract_md5encoder.h
 *
 *  Created on: Mar 31, 2015
 *      Author: Cesar Flores
 */

#ifndef ABSTRACT_MD5ENCODER_H_
#define ABSTRACT_MD5ENCODER_H_

#include <iostream>
#include <fstream>
#include <ctime>
#include <string.h> // to memcpy

#include "md5encoder.h"

using namespace std;

/* An abstract implementation of MD5Encoder, it holds the digest output.*/
class AbstractMD5Encoder : public MD5Encoder {

protected:
    /* The list of calculated digests.*/
    vector<MD5DigestOutput> digests;

protected:
    /*
     * Convert the given raw bytes on printable hexadecimal chars.
     * @param tmpDgstChars, where the printable hexadecimal chars will be stored.
     * @param bytes, the raw bytes to be converted.
     */
    void SetDigestChars(char tmpDgstChars[33], unsigned char bytes[16]);

public:
    /* Default constructor */
	AbstractMD5Encoder();

    /* Destructor */
	virtual ~AbstractMD5Encoder();

    /* @ref MD5Encoder#GetDigests */
    virtual vector<MD5DigestOutput> GetDigests();
};

#endif /* ABSTRACT_MD5ENCODER_H_ */
