#include "md5encoder_factory.h"

MD5Encoder* MD5EncoderFactory::GetMD5Encoder()
{
/* Detects if CUDA is available at compile time.*/
#ifdef NVCC
    return new CU_MD5Encoder();
#else
    return new SimpleMD5Encoder();
#endif
}
