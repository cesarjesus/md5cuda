#ifndef CU_MD5ENCODER_H
#define CU_MD5ENCODER_H

#include <stdio.h>

#include "abstract_md5encoder.h"

/* Parallel implementation of MD5Encoder.*/
class CU_MD5Encoder : public AbstractMD5Encoder
{
public:
	/* Default constructor */
	CU_MD5Encoder();

	/* Destructor */
	virtual ~CU_MD5Encoder();

	/*
	 * Calculate digest for the input list, the MD5 digest will be calculated
	 * in parallel for each element of the input, it means if the input has 10
	 * elements, the 10 digests will be calculated in parallel.
	 * @ref MD5Encoder#MemoryDigest
	 */
	virtual void MemoryDigest(vector<MD5MemoryInput> &inputList);

	/*
	 * Not implemented yet. File operations are 'host' functions and can not be
	 * called from 'device' functions.
	 */
    virtual void FileDigest(vector<string> &fileList);

	/*
	 * Get calculated digests.
	 * @ref MD5Encoder#GetDigests
	 */
	virtual vector<MD5DigestOutput> GetDigests();
};

#endif /* CU_MD5ENCODER_H */
