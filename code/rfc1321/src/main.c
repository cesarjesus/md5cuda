/*
 * main.c
 *
 *  Created on: May 12, 2015
 *      Author: cesar_flores
 */

#include <stdio.h>
#include <string.h>

#include "md5.h"

#define MD 5
#define MD_CTX MD5_CTX
#define MDInit MD5Init
#define MDUpdate MD5Update
#define MDFinal MD5Final

static void MDPrint (digest)
unsigned char digest[16];
{
	unsigned int i;

	for (i = 0; i < 16; i++)
		printf ("%02x", digest[i]);
}

int main(int argc, char** argv)
{
	char* str = "";

	MD_CTX context;
	unsigned char digest[16];
	unsigned int len = strlen (str);

	MDInit (&context);
	MDUpdate (&context, str, len);
	MDFinal (digest, &context);

	printf ("MD%d (\"%s\") = ", MD, str);
	MDPrint (digest);
	printf ("\n");

	return 0;
}
