#include <iostream>
#include <string>
#include <string.h>
#include <stdlib.h>

using namespace std;

void printMultiArray(char** multiArray, int* sizes, int totalElems)
{
    cout << "printing array..." << endl;
    for (int i = 0; i < totalElems; i++)
    {
        cout << "multiArray = " << multiArray[i][0] << endl;
    }
}

int main(int argc, char** argv)
{
    int elems = 3;
    int sizes[elems];
    char* multi_array[elems];

    string s("string that should be a little big trying to exced some limit.");
    multi_array[0] = (char*)malloc(s.size());
    memcpy(multi_array[0], s.c_str(), s.size());

    s = "another text less size the other.";
    multi_array[1] = (char*)malloc(s.size());
    memcpy(multi_array[1], s.c_str(), s.size());

    printMultiArray(multi_array, &sizes, elems);

    free(multi_array[0]);
}
