#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    cout << "Data type sizes..." << endl;

    cout << "char  : " << sizeof(char) << endl;
    cout << "short : " << sizeof(short) << endl;
    cout << "int   : " << sizeof(int) << endl;
    cout << "float : " << sizeof(float) << endl;
    cout << "long  : " << sizeof(long) << endl;
    cout << "double: " << sizeof(double) << endl;

    cout << endl;

    cout << "short int: " << sizeof(short int) << endl;
    cout << "long int : " << sizeof(long int) << endl;

    return 0;
}
