#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>

#define MAX_ELEMENTS 10

using namespace std;

__global__ void kernel(int* d_arr, int* d_arr_square)
{
	int idx = blockIdx.x;
	printf("At block[%d]\n", idx);
	if (idx < MAX_ELEMENTS)
	{
		d_arr_square[idx] = d_arr[idx] * d_arr[idx];
	}
}

void checkError(cudaError_t error)
{
	if (error != cudaSuccess)
	{
		cout << "CUDA Error: " << cudaGetErrorString(error) << endl;
	}
}

__global__ void chrKernel(char** d_chars, char** d_chars_out)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	printf("At block[%d]\n", idx);
	if (idx < MAX_ELEMENTS)
	{
		char* chr = (char*)malloc(sizeof(char) * 2 + 1);
		memset(chr, 0, sizeof(char) * 2 + 1);
		memcpy(chr, d_chars[idx], 2);
		printf("copy print:%s\n", chr);
		printf("direct print:%s\n", d_chars[idx]);
		chr[1] = 'a';
		printf("changed:%s\n", chr);
		memcpy(d_chars_out[idx], chr, 3);
	}
}

int main(int argc, char** argv)
{
	cout << "arrays ..." << endl;

	char* h_chrs[MAX_ELEMENTS];
	string ss("ss");
	for(int i = 0; i < MAX_ELEMENTS; i++)
	{
		h_chrs[i] = (char*)malloc(sizeof(char) * ss.size() + 1);
		memcpy(h_chrs[i], ss.c_str(), ss.size());
	}

	for(int i = 0; i < MAX_ELEMENTS; i++)
	{
		printf("ss[%s]\n", h_chrs[i]);
	}

	cudaError_t error;

	// configure input
	char** d_chrs;
	char** support;

	error = cudaMalloc((void**)&d_chrs, sizeof(char*) * MAX_ELEMENTS);
	checkError(error);

	support = (char**)malloc(sizeof(char*) * MAX_ELEMENTS);

	error = cudaMemcpy(support, d_chrs, sizeof(char*) * MAX_ELEMENTS, cudaMemcpyDeviceToHost);
	checkError(error);

	for(int i = 0; i < MAX_ELEMENTS; i++)
	{
		error = cudaMalloc((void**)&support[i], sizeof(char) * 2 + 1);
		checkError(error);

		error = cudaMemcpy(support[i], h_chrs[i], sizeof(char) * 2 + 1, cudaMemcpyHostToDevice);
		checkError(error);
	}

	error = cudaMemcpy(d_chrs, support, sizeof(char*) * MAX_ELEMENTS, cudaMemcpyHostToDevice);
	checkError(error);

	// configure output
	char** d_chars_out;
	char** support_out;
	error = cudaMalloc((void**)&d_chars_out, sizeof(char*) * MAX_ELEMENTS);
	checkError(error);

	support_out = (char**)malloc(sizeof(char*) * MAX_ELEMENTS);

	error = cudaMemcpy(support_out, d_chars_out, sizeof(char*) * MAX_ELEMENTS, cudaMemcpyDeviceToHost);
	checkError(error);

	for(int i = 0; i < MAX_ELEMENTS; i++)
	{
		error = cudaMalloc((void**)&support_out[i], sizeof(char) * 2 + 1);
		checkError(error);
	}

	error = cudaMemcpy(d_chars_out, support_out, sizeof(char*) * MAX_ELEMENTS, cudaMemcpyHostToDevice);
	checkError(error);

	chrKernel<<<MAX_ELEMENTS, 1>>>(d_chrs, d_chars_out);
	error = cudaDeviceSynchronize();
	checkError(error);

	char** h_chars_out = (char**)malloc(sizeof(char*) * MAX_ELEMENTS);
	//error = cudaMemcpy(h_chars_out, d_chars_out, sizeof(char*) * MAX_ELEMENTS, cudaMemcpyDeviceToHost);
	//checkError(error);
	for (int i = 0; i < MAX_ELEMENTS; i++)
	{
		h_chars_out[i] = (char*)malloc(sizeof(char) * 2 + 1);
		memset(h_chars_out[i], 0, (sizeof(char) * 2 + 1));
		error = cudaMemcpy(h_chars_out[i], d_chars_out[i], sizeof(char) * 2 + 1, cudaMemcpyDeviceToHost);
		checkError(error);
	}

	for(int i = 0; i < MAX_ELEMENTS; i++)
	{
		//cout << "back from device: " << h_chars_out[i] << endl;
	}

	cout << "Finish." << endl;
	/*int h_arr[MAX_ELEMETS];
	for (int i = 0; i < MAX_ELEMETS; i++)
	{
		h_arr[i] = i;
	}

	int* d_arr;
	int* d_arr_square;
	cudaError_t error;

	error = cudaMalloc((void**)&d_arr, sizeof(int) * MAX_ELEMETS);
	checkError(error);

	error = cudaMalloc((void**)&d_arr_square, sizeof(int) * MAX_ELEMETS);
	checkError(error);

	error = cudaMemcpy(d_arr, h_arr, sizeof(int) * MAX_ELEMETS, cudaMemcpyHostToHost);
	checkError(error);

	kernel<<<MAX_ELEMETS,1>>>(d_arr, d_arr_square);
	error = cudaDeviceSynchronize();
	checkError(error);

	int* h_arr_square = (int*)malloc(sizeof(int) * MAX_ELEMETS);
	memset(h_arr_square, 0, sizeof(int) * MAX_ELEMETS);
	error = cudaMemcpy(h_arr_square, d_arr_square, sizeof(int) * MAX_ELEMETS, cudaMemcpyDeviceToHost);
	checkError(error);

	for (int i = 0; i < MAX_ELEMETS; i++)
	{
		cout << h_arr[i] << " * " << h_arr[i] << " = " << h_arr_square[i] << endl;
	}*/

	return 0;
}
