/*
 * utils.cpp
 *
 *  Created on: Apr 11, 2015
 *      Author: cesarjesus
 */

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iomanip>

#include <limits.h>
#include <assert.h>
#include <stdio.h>

using namespace std;

#define BASE_56 56
#define BASE_64 64

/*struct md_buffer{
	unsigned int A;
	unsigned int B;
	unsigned int C;
	unsigned int D;
};*/

// char 00000000
// int  00000000 00000000 00000000 00000000
// for chars it prints ok, but for numbers the bytes are
// inverse.
void printBinary(const char* buffer, size_t size_bytes)
{
	cout << "size in bytes: " << size_bytes << endl;
	char* buffer_ptr = new char[size_bytes];
	memcpy(buffer_ptr, buffer, size_bytes);
	for(unsigned int i = 0; i < size_bytes; i++)
	{
		char b = buffer_ptr[i];
		for(unsigned j = 0; j < CHAR_BIT; j++)
		{
			cout << ((b & 128) ? "1" : "0");
			b <<= 1;
		}
		cout << " ";
	}

	cout << endl;
}

bool isCongruent(size_t size_bytes)
{
	if((size_bytes - BASE_56) % BASE_64 == 0)
	{
		return true;
	}

	return false;
}

size_t calculateMissingBytes(size_t size_bytes)
{
	if (isCongruent(size_bytes))
	{
		return 0;
	}

	return calculateMissingBytes(size_bytes + 1) + 1;
}

// step 1: padding message.
void padding(const char* msg, char* msg_padded, size_t msg_size_bytes, size_t miss_bytes)
{
	memcpy(msg_padded, msg, msg_size_bytes);
	msg_padded[msg_size_bytes] |= 1;
	msg_padded[msg_size_bytes] <<= 7;
	cout << "msg_padded["<< (msg_size_bytes) << "]: " << msg_padded[msg_size_bytes] << endl;
	for(unsigned int i = msg_size_bytes + 1; i < msg_size_bytes + miss_bytes; i++)
	{
		msg_padded[i] &= 0;
		//cout << "msg_padded["<< i << "]: " << msg_padded[i] << endl;
	}
}

// step 2: adding original size of message.
// old message pointer, new message pointer, original size of message, current size of message
void addPreviousMsgSize(const char* oldMsg, char* newMsg, unsigned int origSize, unsigned int currentSize)
{
	memcpy(newMsg, oldMsg, currentSize);
	assert (origSize < LLONG_MAX);
	char* newMsg_ptr = newMsg + currentSize;
	unsigned long long int o_size = origSize;
	// TODO: Possible wrong way to copy, check by less order byte.
	//memcpy(newMsg_ptr, (char*)&o_size, 8);
	char* o_size_ptr = (char*)&o_size;
	int newMsg_count = 0;
	for(int i = 7; i >= 0; i--)
	{
		newMsg_ptr[newMsg_count] = o_size_ptr[i];
		newMsg_count++;
	}
}

// step 3: initialize buffer
void initBuffer(unsigned int md_buffer[])
{
	char* p_ptr;

	// A: 01 23 45 67
	p_ptr = (char*)&md_buffer[0];
	p_ptr[0] = 0x01;
	p_ptr[1] = 0x23;
	p_ptr[2] = 0x45;
	p_ptr[3] = 0x67;

	cout << "A: " << md_buffer[0] << endl;

	// B: 89 ab cd ef
	p_ptr = (char*)&md_buffer[1];
	p_ptr[0] = 0x89;
	p_ptr[1] = 0xab;
	p_ptr[2] = 0xcd;
	p_ptr[3] = 0xef;

	cout << "B: " << md_buffer[1] << endl;

	// C: fe dc ba 98
	p_ptr = (char*)&md_buffer[2];
	p_ptr[0] = 0xfe;
	p_ptr[1] = 0xdc;
	p_ptr[2] = 0xba;
	p_ptr[3] = 0x98;

	cout << "C: " << md_buffer[2] << endl;

	// D: 76 54 32 10
	p_ptr = (char*)&md_buffer[3];
	p_ptr[0] = 0x76;
	p_ptr[1] = 0x54;
	p_ptr[2] = 0x32;
	p_ptr[3] = 0x10;

	cout << "D: " << md_buffer[3] << endl;
}

unsigned int F(unsigned int x, unsigned int y, unsigned int z)
{
	return ((x & y) | ((~x) & z));
}

unsigned int G(unsigned int x, unsigned int y, unsigned int z)
{
	return ((x & z) | (y & (~z)));
}

unsigned int H(unsigned int x, unsigned int y, unsigned int z)
{
	return (x ^ y ^ z);
}

unsigned int I(unsigned int x, unsigned int y, unsigned int z)
{
	return (y ^ (x | (~z)));
}

void fill_t_table(unsigned long t_table[])
{
	unsigned long K = 4294967296;
	double sin_i;
	for(int i = 1; i <= 64; i++)
	{
		sin_i = sin((double)i);
		//cout << "sin(" << i << "): " << sin_i << endl;
		unsigned long t_elem = fabs(sin_i) * K;
		//cout << "t_elem as decimal: " << t_elem << endl;
		//printf("fabs(sin((double)%d)) * %lu = 0x%08lx\n", i, K, t_elem);
		t_table[i - 1] = t_elem;
	}
}

unsigned int FF(unsigned int a, unsigned int b, unsigned int c, unsigned int d, int k, int s, int i)
{
	a = a + F(b,c,d) + k + i;
	a = a << s;
	a = a + b;
	return a;
}

unsigned int GG(unsigned int a, unsigned int b, unsigned int c, unsigned int d, int k, int s, int i)
{
	a = a + G(b,c,d) + k + i;
	a = a << s;
	a = a + b;
	return a;
}

unsigned int HH(unsigned int a, unsigned int b, unsigned int c, unsigned int d, int k, int s, int i)
{
	a = a + H(b,c,d) + k + i;
	a = a << s;
	a = a + b;
	return a;
}

unsigned int II(unsigned int a, unsigned int b, unsigned int c, unsigned int d, int k, int s, int i)
{
	a = a + I(b,c,d) + k + i;
	a = a << s;
	a = a + b;
	return a;
}

int main(int argc, char** argv)
{

#ifdef _LIBC
#	include <endian.h>
#	if __BYTE_ORDER == __BIG_ENDIAN
#		define BIGENDIAN 1
#		printf("BIG ENDIAN SYSTEM")
#	elif
#		define BIGENDIAN 0
#		printf("LITTLE ENDIAN SYSTEM")
#	endif
#endif

	cout << "byte order big endian? " << BIG_ENDIAN << endl;

	int i = 0x3f;
	cout << "to be print in binary: " << i << endl;
	printBinary((char*)&i, sizeof(int));

	char a = 'a';
	cout << "to be print in binary: " << a << endl;
	printBinary((char*)&a, sizeof(char));

	string msg = "";
	cout << "to be print in binary: " << msg.c_str() << endl;
	printBinary(msg.c_str(), msg.length());

	msg = "Hello";
	cout << "to be print in binary: " << msg.c_str() << endl;
	printBinary(msg.c_str(), msg.length());

	char* msgPadded;
	unsigned int msgSize;
	if (!isCongruent(msg.length()))
	{
		size_t miss_bytes = calculateMissingBytes(msg.length());
		msgSize = msg.length() + miss_bytes;
		msgPadded = new char[msgSize];
		cout << "current bytes:" << msg.length() << endl;
		cout << "miss_bytes:" << miss_bytes << endl;
		padding(msg.c_str(), msgPadded, msg.length(), miss_bytes);
		printBinary(msgPadded, msg.length() + miss_bytes);
	}
	char* nnMsg = new char[msgSize + 8];
	cout << "adding size of message" << endl;
	addPreviousMsgSize(msgPadded, nnMsg, msg.length(), msgSize);
	printBinary(nnMsg, msgSize + 8);

	unsigned int md_buffer[4];
	initBuffer(md_buffer);

	unsigned long t_table[64];
	fill_t_table(t_table);

	unsigned long x[64];
	memcpy(x, nnMsg, 64);

	unsigned int AA = md_buffer[0];
	unsigned int BB = md_buffer[1];
	unsigned int CC = md_buffer[2];
	unsigned int DD = md_buffer[3];

	md_buffer[0] = FF(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[0], 7, t_table[0]);
	md_buffer[3] = FF(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[1], 12, t_table[1]);
	md_buffer[2] = FF(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[2], 17, t_table[2]);
	md_buffer[1] = FF(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[3], 22, t_table[3]);

	md_buffer[0] = FF(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[4], 7, t_table[4]);
	md_buffer[3] = FF(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[5], 12, t_table[5]);
	md_buffer[2] = FF(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[6], 17, t_table[6]);
	md_buffer[1] = FF(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[7], 22, t_table[7]);

	md_buffer[0] = FF(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[8], 7, t_table[8]);
	md_buffer[3] = FF(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[9], 12, t_table[9]);
	md_buffer[2] = FF(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[10], 17, t_table[10]);
	md_buffer[1] = FF(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[11], 22, t_table[11]);

	md_buffer[0] = FF(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[12], 7, t_table[12]);
	md_buffer[3] = FF(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[13], 12, t_table[13]);
	md_buffer[2] = FF(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[14], 17, t_table[14]);
	md_buffer[1] = FF(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[15], 22, t_table[15]);


	md_buffer[0] = GG(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[1], 5, t_table[16]);
	md_buffer[3] = GG(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[6], 9, t_table[17]);
	md_buffer[2] = GG(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[11], 14, t_table[18]);
	md_buffer[1] = GG(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[0], 20, t_table[19]);

	md_buffer[0] = GG(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[5], 5, t_table[20]);
	md_buffer[3] = GG(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[10], 9, t_table[21]);
	md_buffer[2] = GG(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[15], 14, t_table[22]);
	md_buffer[1] = GG(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[4], 20, t_table[23]);

	md_buffer[0] = GG(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[9], 5, t_table[24]);
	md_buffer[3] = GG(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[14], 9, t_table[25]);
	md_buffer[2] = GG(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[3], 14, t_table[26]);
	md_buffer[1] = GG(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[8], 20, t_table[27]);

	md_buffer[0] = GG(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[13], 5, t_table[28]);
	md_buffer[3] = GG(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[2], 9, t_table[29]);
	md_buffer[2] = GG(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[7], 14, t_table[30]);
	md_buffer[1] = GG(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[12], 20, t_table[31]);


	md_buffer[0] = HH(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[5], 4, t_table[32]);
	md_buffer[3] = HH(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[8], 11, t_table[33]);
	md_buffer[2] = HH(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[11], 16, t_table[34]);
	md_buffer[1] = HH(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[14], 23, t_table[35]);

	md_buffer[0] = HH(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[1], 4, t_table[36]);
	md_buffer[3] = HH(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[4], 11, t_table[37]);
	md_buffer[2] = HH(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[7], 16, t_table[38]);
	md_buffer[1] = HH(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[10], 23, t_table[39]);

	md_buffer[0] = HH(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[13], 4, t_table[40]);
	md_buffer[3] = HH(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[0], 11, t_table[41]);
	md_buffer[2] = HH(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[3], 16, t_table[42]);
	md_buffer[1] = HH(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[6], 23, t_table[43]);

	md_buffer[0] = HH(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[9], 4, t_table[44]);
	md_buffer[3] = HH(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[12], 11, t_table[45]);
	md_buffer[2] = HH(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[15], 16, t_table[46]);
	md_buffer[1] = HH(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[2], 23, t_table[47]);


	md_buffer[0] = II(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[0], 6, t_table[48]);
	md_buffer[3] = II(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[7], 10, t_table[49]);
	md_buffer[2] = II(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[14], 15, t_table[50]);
	md_buffer[1] = II(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[5], 21, t_table[51]);

	md_buffer[0] = II(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[12], 6, t_table[52]);
	md_buffer[3] = II(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[3], 10, t_table[53]);
	md_buffer[2] = II(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[10], 15, t_table[54]);
	md_buffer[1] = II(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[1], 21, t_table[55]);

	md_buffer[0] = II(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[8], 6, t_table[56]);
	md_buffer[3] = II(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[15], 10, t_table[57]);
	md_buffer[2] = II(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[6], 15, t_table[58]);
	md_buffer[1] = II(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[13], 21, t_table[59]);

	md_buffer[0] = II(md_buffer[0], md_buffer[1], md_buffer[2], md_buffer[3], x[4], 6, t_table[60]);
	md_buffer[3] = II(md_buffer[3], md_buffer[0], md_buffer[1], md_buffer[2], x[11], 10, t_table[61]);
	md_buffer[2] = II(md_buffer[2], md_buffer[3], md_buffer[0], md_buffer[1], x[2], 15, t_table[62]);
	md_buffer[1] = II(md_buffer[1], md_buffer[2], md_buffer[3], md_buffer[0], x[9], 21, t_table[63]);

	md_buffer[0] = md_buffer[0] + AA;
	md_buffer[1] = md_buffer[1] + BB;
	md_buffer[2] = md_buffer[2] + CC;
	md_buffer[3] = md_buffer[3] + DD;

	unsigned char digest[16];

	char* ptr = (char*)&md_buffer[0];
	digest[0] = ptr[3];
	digest[1] = ptr[2];
	digest[2] = ptr[1];
	digest[3] = ptr[0];

	ptr = (char*)&md_buffer[1];
	digest[4] = ptr[3];
	digest[5] = ptr[2];
	digest[6] = ptr[1];
	digest[7] = ptr[0];

	ptr = (char*)&md_buffer[2];
	digest[8] = ptr[3];
	digest[9] = ptr[2];
	digest[10] = ptr[1];
	digest[11] = ptr[0];

	ptr = (char*)&md_buffer[3];
	digest[12] = ptr[3];
	digest[13] = ptr[2];
	digest[14] = ptr[1];
	digest[15] = ptr[0];

	cout << "raw digest: " << digest << endl;
	cout << "hex digest: ";
	for(int i = 0; i < 16; i++){
		printf("%02x", digest[i]);
	}
	cout << endl;

	// -------------------------------

	/*msg = "exactly size on bytes to be congruent to base number....";
	msgSize = msg.length();
	msgPadded = new char[msg.length()];
	if (isCongruent(msg.length()))
	{
		memcpy(msgPadded, msg.c_str(), msg.length());
		cout << "it is already congruent" << endl;
		printBinary(msgPadded, msg.length());
	}
	nnMsg = new char[msgSize + 8];
	cout << "adding size of message" << endl;
	addPreviousMsgSize(msgPadded, nnMsg, msg.length(), msgSize);
	printBinary(nnMsg, msgSize + 8);

	msg = "something more bigger that should be taking account to calculate"
			" the padding.";

	if (!isCongruent(msg.length()))
	{
		size_t miss_bytes = calculateMissingBytes(msg.length());
		msgSize = msg.length() + miss_bytes;
		msgPadded = new char[msgSize];
		cout << "current bytes:" << msg.length() << endl;
		cout << "miss_bytes:" << miss_bytes << endl;
		padding(msg.c_str(), msgPadded, msg.length(), miss_bytes);
		printBinary(msgPadded, msg.length() + miss_bytes);
	}
	nnMsg = new char[msgSize + 8];
	cout << "adding size of message" << endl;
	addPreviousMsgSize(msgPadded, nnMsg, msg.length(), msgSize);
	printBinary(nnMsg, msgSize + 8);

	unsigned long long int j = 1;
	cout << "trying to print 64 bits integer: " << j << endl;
	printBinary((char*)&j, sizeof(j));*/

	return 0;
}


/*
 *
00011100 00110010 00001000 11100000

LE					BE
00000000 00000000 00000000 00011100	00011100 00000000 00000000 00000000	00011100 00000000 00000000 00000000

00000000 00000000 00000000 00110010	00110010 00000000 00000000 00000000
00000000 00000000 00110010 00000000	00000000 00000000 00000000 00000000	00000000 00000000 00000000 00110010

00000000 00000000 00000000 00001000	00001000 00000000 00000000 00000000
00000000 00001000 00000000 00000000	00000000 00000000 00000000 00000000	00000000 00000000 00001000 00000000

00000000 00000000 00000000 11100000	11100000 00000000 00000000 00000000
11100000 00000000 00000000 00000000	00000000 00000000 00000000 00000000	00000000 11100000 00000000 00000000


11100000 00001000 00110010 00011100	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	00011100 11100000 00001000 00110010
 *
 * */
