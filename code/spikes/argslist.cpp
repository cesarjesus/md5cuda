#include <iostream>
#include <string>
#include <vector>
#include <stdarg.h>

using namespace std;

typedef struct
{
    unsigned char* ptr;
    unsigned int length;
} Couple;

void printStrings(const char* s, ...)
{
    
}

void printVectorStrings(vector<string> &strs)
{
    for(vector<string>::iterator it = strs.begin(); it != strs.end(); it++)
    {
	cout << *it << endl;
    }
}

void printVectorCouples(vector<Couple> &couples)
{
    for(vector<Couple>::iterator it = couples.begin(); it != couples.end(); it++)
    {
	cout << "str[" << (*it).ptr << "] size[" << (*it).length << "]" << endl;
    }
}

vector<Couple> GetCouples()
{
    Couple c;
    string msg("I'm message.");
    c.ptr = (unsigned char*)msg.c_str();
    c.length = msg.size();

    vector<Couple> couples;
    couples.push_back(c);

    return couples;
}

int main(int argc, char** argv)
{
    cout << "playing with variadic functions..." << endl;

    string s("s");
    string s1("s1");
    string s2("s2");

    vector<string> v;
    v.push_back(s);

    printVectorStrings(v);

    vector<Couple> c;
    Couple c1;
    c1.ptr = (unsigned char*)s2.c_str();
    c1.length = s2.size();

    c.push_back(c1);
    printVectorCouples(c);

    vector<Couple> c2 = GetCouples();

    printVectorCouples(c2);

    printStrings(s.c_str());
    printStrings(s.c_str(),s1.c_str());
    printStrings(s.c_str(),s1.c_str(),s2.c_str());

    return 0;
}
