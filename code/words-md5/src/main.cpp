#include <iostream>
#include <ctime>

using namespace std;

void delay()
{
	for (int i = 0; i < 999999; i++) ;
}

int main(int argc, char** argv)
{
	cout << "time tests..." << endl;

	clock_t start = clock();

	delay();

	cout << float(clock() - start) / CLOCKS_PER_SEC << endl;

	return 0;
}
